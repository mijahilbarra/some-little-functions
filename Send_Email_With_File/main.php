<?php

require_once 'PHPMailerAutoload.php';



$name = isset($_POST['name']) ? $_POST['name'] : false;

$email = isset($_POST['email']) ? $_POST['email'] : false;

filter_var($email, FILTER_VALIDATE_EMAIL);

$message = isset($_POST['message']) ? $_POST['message'] : false;

$file = isset($_FILES['file']) ? $_FILES['file'] : false;

//VALIDATE FILE, ALLOW FORMAT
if($file){

    $format_allow =  array('pdf');
    $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
    if(!in_array($ext,$format_allow) ) {
        $result = [
            'message' => 'El formato del archivo no es admitido.',
        ];
        echo json_encode($result);
        return;
    }

}

//VALIDATE DATA, NOT EMPTY, NOT FALSE
if(!$name && $name=!'' || !$email && $email=!'' || !$message && $message=!'' ) {
    $result = [
        'message' => 'Los datos están incompletos.',
    ];
    echo json_encode($result);
    return;
}

//MESSAGE BODY
$body = "
<div >

<h2 style='color: rgb(74, 74, 74); margin-top: 30px; margin-bottom: 30px;'>Nuevo mensaje</h2>
</div>
<div>
<h3><small>De:</small> $name </h3>
<h3><small>Correo:</small> $email</h3>
<p>$message</p>
</div>
";


$mail = new PHPMailer;

//SMTP DEFINITION
//IMPORTANT! These definitions is only in Localhost. You will connect with your mail services to test email sending. Remember set permits in your mail services
//$mail->IsSMTP();
//$mail->SMTPAuth = true;
//$mail->SMTPDebug = 2;
//$mail->Host = "smtp.gmail.com";
//$mail->Port = 587;
//$mail->SMTPSecure = 'tls'; 

//$mail->Username = "un_correo_gmail@gmail.com";
//$mail->Password = "la_respectiva_clave";


//UPLOAD FILE IF IT EXISTS

if($file && $file != ''){
    $mail->AddAttachment($file['tmp_name'],$file['name']);
}


//MESSAGES DATA

$mail->From       = 'info@sermep.com.pe';

$mail->FromName   = 'Sermep Contacto';

$mail->Subject    = 'Formulario de contacto de la web';

$mail->Body       = $body;

$mail->AddAddress($email);

$mail->IsHTML(true);

$mail->CharSet    = "UTF-8";

$mail->SMTPDebug = 0;

$mail->AddBCC("info@sermep.com.pe", "Contacto");


//MESSAGE ABOUT SUCCESS

$result = [
    'message' => (!$mail->send())? $mail->ErrorInfo : 'Mensaje enviado con éxito.',
];

echo json_encode($result);