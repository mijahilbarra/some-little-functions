var config = {
    apiKey: "AIzaSyBD9P6fLtxEVFq39P1v2q1B1I5-RlSc5KY",
    authDomain: "site-mija.firebaseapp.com",
    databaseURL: "https://site-mija.firebaseio.com",
    projectId: "site-mija",
    storageBucket: "site-mija.appspot.com",
    messagingSenderId: "215343299538"
  };
firebase.initializeApp(config);

const app_especialidad = new Vue({
  el: '#myPag',
  data: {
    host: "https://trujillo-hackspace.herokuapp.com",
    login: {
        id: '',
        password: '',
    },
    post:{
        id: '',
        tag: '',
        title: '',
        content: '',
        image: '',
        data1: '',
        data2: '',
        data3: '',
    },
    mydata:{},
    show_form_singIn: true,
    editableId:'',

  },
    methods:{
        signIn: function(){

            let _this = this

            let btnMessage = $('#signIn').find('input[type="submit"]')
            btnMessage.attr('value', 'Cargando...')
            
            firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL) 
                .then(

                    firebase.auth().signInWithEmailAndPassword(_this.login.id, _this.login.password)
                        .then(()=>{
                            _this.verifySession()
                        })
                        .catch(function(error) {
                            var errorCode = error.code
                            var errorMessage = error.message
                            btnMessage.attr('value', errorCode)
                        })
                )
            
            
        },
        signOut: function(){
            firebase.auth().signOut().then(function() {
                // Sign-out successful.
              }).catch(function(error) {
                // An error happened.
              });
        },
        verifySession: function(){
            var user = firebase.auth().currentUser
            if (user) {
                this.show_form_singIn = false
                console.log('Sesión iniciada')
            } else {
                this.show_form_singIn = true
                console.log('Sin sesión')
            }
        },
        createData: function(){
            let btnMessage = $('#newData').find('input[type="submit"]')
            btnMessage.attr('value', 'Enviando información...')
            let _this = this
            
            let newPostKey = (this.post.id == '') ? firebase.database().ref().child(this.post.tag).push().key : this.post.id

            let postData = {
                id: newPostKey,
                tag: this.post.tag,
                title: this.post.title,
                content: this.post.content,
                image: this.post.image,
                data1: this.post.data1,
                data2: this.post.data2,
                data3: this.post.data3,
            }

            let updates = {}
            updates[newPostKey] = postData

            firebase.database().ref().child(this.post.tag).update(updates)
            .then(()=>{
                btnMessage.attr('value', 'Información enviada')
                _this.post.tag = ''
                _this.post.title = ''
                _this.post.content = ''
                _this.post.image = ''
                _this.post.data1 =''
                _this.post.data2 = ''
                _this.post.data3 = ''
                _this.post.id = ''

                _this.readData()
            })
            .catch((error)=>{
                console.log(error)
            })
        },

        readData: function(){
            let _this = this
            var myInfo = firebase.database().ref()
            myInfo.on('value', function(snapshot) {
                _this.mydata = snapshot.val()
            })
            
        },

        updateData: function(id, tag){           
            let _this = this
            var myInfo = firebase.database().ref(`${tag}/${id}`)
            myInfo.on('value', function(snapshot) {
                _this.post = snapshot.val()
                _this.post.id = id
                
            })

        },

        deleteData: function(id, tag){
            firebase.database().ref(`${tag}/${id}`).remove()
            .then(()=>{
                console.log('Eliminado')
            })
            .catch((error)=>{
                console.log(error)
            })
        }
    },
    created: function(){
        this.verifySession()
        this.readData()
    }
})