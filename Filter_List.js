$(()=>{
    //Set a MainBox, where input Search and list Search are
    let searchElement = (mainBox)=>{
        
    //Identify a input Search  
    let input_search = mainBox.find('.input-search')
    
    //After key 
    input_search.keyup(()=>{
        
        //Identify element list by Class name
        let elementList = mainBox.find('.name')
      
        //Identify list box by Class nameBox and hide it.
        $('.name').parents('.nameBox').hide()
        
        //Walk around each element list
        elementList.each(function(){
            
            //Define population, where will you search
            let population = $(this).html().toLowerCase()
            
            //Define sample, what will you search
            let sample = fuzzy_search.val().toLowerCase()
            
            //Compare sample in population, -1 to not found
            if(population.indexOf(sample)>-1||toEnglish(population).indexOf(sample)>-1){
                //Show what did you search
                $(this).parents('.nameBox').show()
            } 
        });
    })
}
    //In Spanish, something people do not write with Latin Signs
    let toEnglish = (string) => {
        string = string.replace(/á/gi,"a");
        string = string.replace(/é/gi,"e");
        string = string.replace(/í/gi,"i");
        string = string.replace(/ó/gi,"o");
        string = string.replace(/ú/gi,"u");
        return string
    }
    
    //Do not forget iniciate function
    searchElement($('#some_main_div'))
})