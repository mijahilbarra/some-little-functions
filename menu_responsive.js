$(()=>{

    //SHOW SUBTITLES

    let showSubtitles = (title) => {

        title.children('.menuSubTitle').show(250)

        title.css('color', '#e24545')

        title.addClass('active')

    }

    //HIDE SUBTITLES

    let hideSubtitles = () => {

        $('.menuSubTitle').hide(250)

        $('.menuTitle').css('color', '#888')

        $('.menuTitle').removeClass('active') 

    }

    //Define function if exist a ClassName menuTitle with or without Type="desktop"

    let  menuFunction = () => {

        $('.menuTitle').each(function(){

            $(this).on('click', ()=>{

                if($(this).attr('type') === 'mobile'){

                    let isActive = $(this).hasClass('active')

                    hideSubtitles()

                    if(!isActive){

                        showSubtitles($(this))

                    }  

                }

            })

        })

        $('.menuTitle[type="desktop"]').each(function(){

            $(this).hover(()=>{

                if($(this).attr('type') === 'desktop'){
                    showSubtitles($(this))
                }

            }, ()=>{

                if($(this).attr('type') === 'desktop'){

                    hideSubtitles()

                }

            })

        })         

    }    

    //BUTTON TO ACTIVE MENU FOR MOVILE SCREEN

    let btnMenu = $('#btnNav')

    btnMenu.on('click', ()=>{

        let menuNav = $('.menuNav')

        let isActive = btnMenu.hasClass('active')

        menuNav.hide(250)

        btnMenu.removeClass('active')

        if(!isActive){

            menuNav.show(250)

            btnMenu.addClass('active')

        }

    })
    
    //Change tag of menuTitle when Width window is changed.

    let isMobile = $(window).width() <= 760

    $('.menuTitle').attr('type', (isMobile) ? 'mobile' : 'desktop' ) 

    $(window).resize(()=>{

        isMobile = $(window).width() <= 760

        $('.menuTitle').attr('type', (isMobile) ? 'mobile' : 'desktop' ) 

    })

    menuFunction() 

})